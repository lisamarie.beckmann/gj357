# GJ357

Hier finden sich die Streudiagramm-Matrizen für die verschiedenen Durchläufe:  
Kepler Orbits, freie Inklination: Kepinc  
Zirkuläre Orbits, freie Inklination: Circinc  
Kepler Orbits, feste Inklination: Kepfixinc  
   
Planet 0 beschreibt dabei den innersten Planeten mit Transit (GJ 357 b), Planet 1 GJ 357 c und Planet 2 GJ 357 d
